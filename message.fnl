(local lume (require :lib.lume))

;; a message is any object on the map that is marked as collidable and sensor
;; (booleans) and has a custom property called msg.
;; that message will be displayed once for 4 seconds upon entry of that object.

;; placing an "unless" string property on the object means the message won't
;; be shown if the item in that property has been picked up.

(local seen {})
(var current nil)
(var t 0)

(fn draw []
  (when current
    (love.graphics.setColor 1 1 1)
    (love.graphics.printf current 48 8 256)))

(fn update [dt]
  (set t (if current (+ t dt) 0))
  (when (> t 4) (set current nil)))

(fn init []
  (lume.clear seen)
  (set current nil))

(fn show [msg]
  (when (not (. seen msg))
    (set (current) (values msg 0))
    (tset seen msg true)))

(fn find [a-data b-data]
  (or (and a-data.properties a-data.properties.msg b-data.blob a-data)
      (and b-data.properties b-data.properties.msg a-data.blob b-data)))

{:init init :draw draw :update update :show show :find find}
