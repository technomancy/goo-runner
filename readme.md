# Goo Runner

A game for the [Lisp Game Jam Autumn 2018](https://itch.io/jam/autumn-lisp-game-jam-2018).

Written in the [Fennel programming language](https://fennel-lang.org)
using the [LÖVE](https://love2d.org) framework.

## Install

[Downloads are available on itch.io](https://technomancy.itch.io/goo-runner). To
run from source, first install version 11.1 of [LÖVE](https://love2d.org)
and run `make run`.

## Controls

* Arrows to move
* F11 to toggle fullscreen
* M to toggle mute
* escape to quit

## Credit
All of the wonderful art comes from other folks. You can find all sources below:

* [SciFi roguelike enemies](https://opengameart.org/content/3x-updated-32x32-scifi-roguelike-enemies)
* [Sci-fi 32x32 Tileset by Viktor Hahn](https://opengameart.org/content/32x32-sci-fi-tileset)
* [Key cards by XCVG](https://opengameart.org/content/keycredit-cards)
* [Boxes by Alekei](https://opengameart.org/content/box-2)
* [Fixedsys Excelsior font](http://www.fixedsysexcelsior.com/)
* [Music by Drozerix](https://modarchive.org/index.php?request=view_by_moduleid&query=173085)

These libraries were also invaluable:

* [LÖVE](https://love2d.org) game framework
* [Fennel](https://fennel-lang.org) compiler
* [LuaJIT](https://luajit.org) JIT compiler/VM/runtime
* [lume](https://github.com/rxi/lume) utility library
* [simple tiled implementation](https://github.com/Karai17/Simple-Tiled-Implementation)
* [loveblobs](https://github.com/exezin/loveblobs) soft body physics implementation

The [Tiled](https://www.mapeditor.org) map editor was also used.

## License

© 2018 Emma Bukacek and Phil Hagelberg

Distributed under the GNU General Public License version 3 or later; see file license.txt.
