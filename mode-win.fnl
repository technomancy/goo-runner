(local lume (require :lib.lume))
(local (w h) (love.window.getMode))

(local img (love.graphics.newImage "assets/win.jpg"))
(local (iw ih) (: img :getDimensions))
(var t 1)

(local msgs ["You've done it."
             "You escaped."
             "The whole world awaits you."
             "Now go... and do slimey things."])

(local offsets {1 0.2 2 0.3 3 0.35 4 0.375})

(fn draw []
  (let [msg-count (math.min 4 (math.floor t))
        zoom (lume.lerp 0.2 1.0 (/ msg-count 4))
        offset (. offsets msg-count)]
    (love.graphics.setColor 1 1 1)
    (love.graphics.push)
    (love.graphics.draw img 0 0 0 zoom zoom
                        (* iw offset)
                        (* ih offset 1.25))
    (love.graphics.pop)
    (love.graphics.setColor 0 0.9 0)
    (for [i 1 msg-count]
      (love.graphics.print (. msgs i) 64 (+ 32 (* i 32))))))

(fn update [dt] (set t (+ t dt)))

{:update update :draw draw
 :keypressed (fn [key]
               (when (= key :r)
                 (set t 1)))}
