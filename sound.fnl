(local sounds
       {:music (love.audio.newSource "assets/porn_industryy.xm" :stream)})

(: sounds.music :setLooping true)

(fn toggle [name]
  (if (love.filesystem.getInfo "mute")
      (do (love.filesystem.remove "mute")
          (: (. sounds (or name :music)) :play))
      (do (love.filesystem.write "mute" "true")
          (each [_ sound (pairs sounds)]
            (: sound :pause)))))

{:play (fn play [name]
         (when (and (not (: (. sounds name) :isPlaying))
                    (not (love.filesystem.getInfo "mute")))
           (: (. sounds name) :play)))
 :toggle toggle}
