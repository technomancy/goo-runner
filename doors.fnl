(local utils (require :utils))

(fn get-door [world name]
  (let [body (lume.match (: world :getBodies)
                         (fn [b] (. (or (: b :getUserData) {}) :map)))]
    (lume.match (: body :getFixtures)
                (fn [f] (= (. (or (: f :getUserData) {}) :name) name)))))

(local colors {:blue [0.2 0.2 0.6 0.9]
               :red [0.6 0.2 0.2 0.9]
               :green [0.2 0.6 0.2 0.9]})

(fn get-color [f]
  (let [data (: f :getUserData)]
    (or (. colors data.name) [0.2 0.2 0.2])))

(fn draw [world]
  (let [body (lume.match (: world :getBodies)
                         (fn [b] (. (or (: b :getUserData) {}) :map)))]
    (each [_ f (ipairs (: body :getFixtures))]
      (when (and (= (. (or (: f :getUserData) {}) :type) :door)
                 (not (: f :isSensor)))
        (let [(x1 y1 x2 y2) (: f :getBoundingBox)]
          (love.graphics.setColor (get-color f))
          (love.graphics.rectangle "fill" x1 y1 (- x2 x1) (- y2 y1)))))))

(fn init [map world]
  (let [body (lume.match (: world :getBodies)
                         (fn [b] (. (or (: b :getUserData) {}) :map)))]
    (each [_ f (ipairs (: body :getFixtures))]
      (when (= (. (or (: f :getUserData) {}) :type) :door)
        (tset (: f :getUserData) :door true)
        (: f :setSensor false))))
  (let [layer (utils.get-layer map :doors)]
    (set layer.draw (partial draw world))))

(fn update-door [world f data]
  nil)

(fn update [map world dt]
  (let [body (lume.match (: world :getBodies)
                         (fn [b] (. (or (: b :getUserData) {}) :map)))]
    (each [_ f (ipairs (: body :getFixtures))]
      (let [data (: f :getUserData)]
        (when data.door
          (update-door world f data))))))

(fn toggle [world name force-on]
  (let [fixture (get-door world name)]
    (: fixture :setSensor (or force-on (not (: fixture :isSensor))))))

(fn open [world door]
  (: door :setSensor true))

(fn one-of [world a b]
  (if (get-door world a.name) a (get-door world b.name) b))

{:init init :update update :toggle toggle :open open :one-of one-of}
