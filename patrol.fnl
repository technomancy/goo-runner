(local lume (require :lib.lume))
(local utils (require :utils))

(local prox 5) ; at what distance do you consider "arrived" at target?

(fn move [body path accel]
  (let [target (or (coroutine.yield) (. path 1))
        (x y) (: body :getPosition)
        tx (. target :x) ty (. target :y)
        dx (- tx x) dy (- ty y)
        (vx vy) (: body :getLinearVelocity)
        theta (math.atan2 dy dx)
        fx (* accel (math.cos theta))
        fy (* accel (math.sin theta))
        reached-target? (< (lume.distance x y tx ty) prox)]
    (: body :setAngle theta)
    (when reached-target?
      (table.insert path (table.remove path 1)))
    (: body :applyLinearImpulse (- fx vx) (- fy vy))
    (move body path accel)))

;; Given a body with a patrol-id, return patrol that matches the provided id
(fn get-body-patrol [body map]
  (let [patrol-id (. (: body :getUserData) :patrol :id)]
    (if patrol-id
      (let [patrol-layer (utils.get-layer map "patrols")]
        (lume.match (. patrol-layer "objects")
                    (fn [p] (= p.id patrol-id)))))))

;; Sets initial patrol state for body given its patrol-id
(fn start-patrol [body map]
  ;; assume every enemy must have a patrol
  (let [patrol (assert (get-body-patrol body map) "no patrol found")
        path (assert (. patrol :polygon) "no path for patrol")
        accel (if (= (. (: body :getUserData) :type) :fast) 75 50)]
    (set patrol.move (coroutine.wrap (partial move body
                                              (lume.clone path) accel)))))

;; Moves the body for one frame according to their patrol
(fn update-patrol [body map dt]
  (let [patrol (get-body-patrol body map)]
    (patrol.move dt)))

{:start-patrol start-patrol
 :update-patrol update-patrol}
