(local softbody (require :lib.loveblobs.softbody))
(local tiled (require :lib.tiled))

(local doors (require :doors))
(local enemy (require :enemy))
(local furniture (require :furniture))
(local inventory (require :inventory))
(local lint (require :lint))
(local utils (require :utils))
(local message (require :message))

;; a whole new wooooooooorld 𝅘𝅥𝅮
(local world (love.physics.newWorld 0 0 true))
(love.physics.setMeter 32)
(local map (lint (tiled "map.lua" ["box2d"])))
(global w world) (global m map) ; for repl

(: map :box2d_init world)

(local max-blobs 4)
(var selected 1)

(local (w h) (love.window.getMode))
(local accel 92)
(local speed-limit 92)
(local camera {:tx 740 :ty 680})

(fn make-blob [x y r]
  (let [blob (softbody world x y r)]
    (: blob :setFrequency 1)
    (: blob :setDamping 12)
    (: blob :setFriction 1)
    (: blob.centerBody :setLinearDamping 1)
    blob))

(local (player-layer, player-layer-index) (utils.get-layer map "player"))
(local player-start (-> (. player-layer "objects")
                        (lume.match (fn [o] (= o.type "player")))))

(local blobs [])

(var killed? false)
(var won? false)
(var (fade-blob fade-screen) (values 0.8 0.5))

(let [layer (: map :addCustomLayer "blobs" player-layer-index)]
  (set layer.draw
       (fn []
         (each [i blob (ipairs blobs)]
           (love.graphics.setLineWidth 1)
           (love.graphics.setColor 0 (if (= selected i)
                                         0.6 0.8) 0)
           (: blob :draw "fill")
           (love.graphics.setColor 0 0.9 0)
           (: blob :draw "line")
           (when killed?
             (love.graphics.setColor 0 fade-blob 0)
             (: blob :draw "fill"))))))

(fn draw []
  (: map :draw (- camera.tx) (- camera.ty))
  (when (love.keyboard.isDown "tab")
    (: map :box2d_draw (- camera.tx) (- camera.ty)))
  (inventory.draw)
  (message.draw)
  (when killed?
    (love.graphics.setColor fade-screen fade-screen fade-screen
                            (- 1 fade-screen))
    (love.graphics.rectangle "fill" 0 0 w h))
  (when won?
    (love.graphics.setColor fade-screen fade-screen fade-screen fade-screen)
    (love.graphics.rectangle "fill" 0 0 w h)))

(fn split []
  (let [blob (. blobs selected)
        (x y) (: blob.centerBody :getPosition)
        r (* blob.radius 0.75)
        b1 (make-blob (+ x (* r 0.5)) y r)
        b2 (make-blob (- x (* r 0.5)) y r)]
    (: blob :destroy)
    (: b1.centerBody :applyLinearImpulse 12 12)
    (: b2.centerBody :applyLinearImpulse -12 -12)
    (table.remove blobs selected)
    (table.insert blobs selected b1)
    (table.insert blobs selected b2)))

(fn distance [a b]
  (if (or (not a.centerbody) (not b.centerbody) (= a b)) false
      (let [(x1 y1) (: a.centerBody :getPosition)
            (x2 y2) (: b.centerBody :getPosition)]
        (lume.distance x1 y1 x2 y2))))

(fn join []
  (when (> (# blobs) 1) ; TODO: also limit distance
    (let [blob (. blobs selected)
          closest (. (lume.sort blobs (partial distance blob)) 1)
          (x y) (: blob.centerBody :getPosition)
          newblob (make-blob x y (* 1.5 blob.radius))]
      (table.remove blobs selected)
      (table.remove blobs (lume.find blobs closest))
      (: blob :destroy)
      (: closest :destroy)
      (table.insert blobs newblob)
      (set selected (math.max 1 (lume.find blobs newblob))))))

(fn camera-follow-blob [blob zoom-scale]
  (let [points (: blob :getPoints)
        (x y) (: blob.centerBody :getPosition)
        (win-width, win-height) (: love.window :getMode)]
    (set camera.tx (- x (/ win-width 2 zoom-scale)))
    (set camera.ty (- y (/ win-height 2 zoom-scale)))))

(fn begin-contact [a b coll]
  (let [a-data (: a :getUserData)
        b-data (: b :getUserData)
        killer? (or (and a-data.enemy b-data.blob)
                    (and b-data.enemy a-data.blob))
        winner? (or (and a-data.properties a-data.properties.goal b-data.blob)
                    (and b-data.properties b-data.properties.goal a-data.blob))]
    (when (and (love.keyboard.isDown "lshift")
               (not (and a-data.blob b-data.blob)))
      (print :collision-between a b)
      (pp a-data)
      (pp b-data))
    ; (when (and a-data.blob b-data.furniture)
    ;   (let [(x y) (: (: a :getBody) :getLinearVelocity)]
    ;       (: (: b :getBody) :applyForce x y)))
    ; (when (and b-data.blob a-data.furniture)
    ;   (let [(x y) (: (: b :getBody) :getLinearVelocity)]
    ;       (: (: a :getBody) :applyTorque x y)))
    (let [item (inventory.item? a-data b-data)]
      (when item
        (inventory.pickup map world item.name)
        (message.show (.. "picked up " item.name " keycard"))))
    (let [door-data (doors.one-of world a-data b-data)]
      (when (and door-data (inventory.openable? door-data))
        (doors.open world (if (= door-data a-data) a b))))
    (when winner?
      (set won? true))
    (when (and killer? (not won?))
      (set killed? true))
    (let [msg (message.find a-data b-data)]
      (when (and msg (not (inventory.has? msg.properties.unless)))
        (message.show msg.properties.msg)))))

(fn pre-solve [a b coll]
  (let [a-data (: a :getUserData)
        b-data (: b :getUserData)
        enemy-push? (or (and a-data.enemy b-data.furniture)
                        (and b-data.enemy a-data.furniture))]
    ;; prevent enemies from pushing furniture
    ;; TODO: this stops an enemy from moving *away* from furniture too
    (-> a (: :getBody) (: :setLinearVelocity 0 0))
    (-> b (: :getBody) (: :setLinearVelocity 0 0))))

(: world :setCallbacks begin-contact nil pre-solve)

(fn init []
  (each [_ blob (ipairs blobs)] (: blob :destroy))
  (lume.clear blobs)
  (table.insert blobs (make-blob (. player-start "x")
                                 (. player-start "y") 20))
  (set selected 1)
  (set (killed? won? fade-blob fade-screen) (values false false 0.8 0.5))
  (enemy.init map world)
  (furniture.init map world)
  (doors.init map world)
  (inventory.init map world)
  (message.init))

(local touch-accel 16)

(fn touchmoved [id x y dx dy pressure]
  (when (not killed?)
    (: (. blobs selected :centerBody) :applyLinearImpulse
       (* dx touch-accel) (* dy touch-accel))))

{:init init
 :draw draw
 :update (fn [dt set-mode zoom-scale]
           (: world :update dt)
           (message.update dt)
           (doors.update map world dt)
           (each [_ blob (ipairs blobs)]
             ;; reduce smearing by running blob physics at finer granularity
             (let [times 32
                   dt (/ dt times)]
               (for [_ 1 times]
                 (: blob :update dt))))
           (when (= 0 (# blobs)) (init)) ; for reloading
           (let [blob (. blobs selected)
                 (x y) (: blob.centerBody :getPosition)
                 speed (lume.distance 0 0 (: blob.centerBody :getLinearVelocity))]
             (enemy.update map world blob dt)
             (var (dx dy) (values 0 0))
             (when (love.keyboard.isDown "up") (set dy (* dt accel -1)))
             (when (love.keyboard.isDown "down") (set dy (* dt accel)))
             (when (love.keyboard.isDown "left") (set dx (* dt accel -1)))
             (when (love.keyboard.isDown "right") (set dx (* dt accel)))
             (when (and (not killed?) (< speed speed-limit))
               (: blob.centerBody :applyLinearImpulse dx dy))
             (camera-follow-blob blob zoom-scale))
           (when killed?
             (set fade-screen (math.max 0 (- fade-screen (/ dt 4))))
             (set fade-blob (math.max 0 (- fade-blob (/ dt 2))))
             (when (= fade-screen 0)
               (set-mode :die)))
           (when won?
             (set fade-screen (math.min 1 (+ fade-screen (/ dt 8))))
             (when (= fade-screen 1)
               (set-mode :win))))
 :keypressed (fn [key set-mode]
               (when (<= 1 (or (tonumber key) 0) (# blobs))
                 (set selected (tonumber key)))
               ;; (when (and (= key "space") (< (# blobs) max-blobs))
               ;;   (split))
               (when (or (= key "lshift") (= key "rshift") (= key "shift"))
                 (join))
               (when (= key :r)
                 (love.load)))
 :touchmoved touchmoved}
