(local lume (require :lib.lume))
(local utils (require :utils))

(local sprites (love.graphics.newImage "assets/lab-tileset.png"))
(fn quad [x y w h] )

;; each object in the map has a "gid" that indicates which tile in the tileset
;; it's represented by. we use that to map to quads and shapes.

;; Because Tiled is 0 indexed, be sure to add one to the GID value
(local gids {49 :table1 52 :table2 55 :table3 58 :table4
             142 :chair 220 :sofa1 221 :sofa2 479 :brown-box
             480 :grey-box 511 :red-box})

;; Each coordinate carries this shape:
;;   [x y w h collision-h]
;;
;;   x - top-left x value of base furniture tile
;;   y - top-left y value of base furniture tile
;;   w - width of entire furniture
;;   h - height of entire furniture
;;   collision-h - height of collidable part of furniture
(local coords {:brown-box [960 448 32 32 32]
               :chair [420 117 24 28 43]
               :grey-box [992 448 32 32 32]
               :red-box [960 480 32 32 32]
               :sofa1 [868 160 26 64 64]
               :sofa2 [898 160 26 64 64]
               :table1 [508 29 40 24 36]
               :table2 [604 8 40 44 56]
               :table3 [680 18 80 34 44]
               :table4 [776 18 80 34 44]})

(local quads {}) ; a place to cache quads when looking up and lazily creating

(fn get-quad [gid]
  (or (. quads gid)
      (let [name (. gids gid)
            [x y w _ h] (assert (. coords name) (.. "unknown gid: " gid))
            quad (love.graphics.newQuad x y w h (: sprites :getDimensions))]
        (tset quads gid quad)
        quad)))

(fn add-furniture [furniture world]
  (let [body (love.physics.newBody world furniture.x furniture.y :dynamic)
        name (. gids furniture.gid)
        [_ _ w h] (. coords name)
        shape (love.physics.newRectangleShape w h)
        fixture (love.physics.newFixture body shape)]
    (: fixture :setFriction 16)
    (: fixture :setUserData {:furniture true})
    (: body :setUserData {:furniture true :w w :h h :gid furniture.gid})
    (: body :setLinearDamping 16)
    (: body :setAngularDamping 3)
    (: body :setMass 0.01)))

(fn draw [world]
  (each [_ body (ipairs (utils.get-bodies world :furniture))]
    (let [(x y) (: body :getPosition)
          data (: body :getUserData)]
      (love.graphics.draw sprites (get-quad data.gid)
                          (math.floor x)
                          (math.floor y)
                          0 1 1 (/ data.w 2) (/ data.h 2)))))

(fn init [map world]
  (utils.clear world :furniture)
  (let [(starts-layer layer-index) (utils.get-layer map :furniture-starts)
        layer (or (utils.get-layer map :furniture)
                  (: map :addCustomLayer :furniture layer-index))]
    (each [_ furniture (ipairs (. starts-layer :objects))]
      (add-furniture furniture world))
    (set layer.draw (partial draw world))))

{:init init}
