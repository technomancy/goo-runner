# Isometric
* http://www.freedroid.org/gallery/screenshots/


# Topdown

Map
---
* https://opengameart.org/content/hospital-room                                                                             │
* https://opengameart.org/content/lab-textures
* https://opengameart.org/content/lpc-skorpios-scifi-sprite-pack
* https://opengameart.org/content/basic-32x32-sci-fi-tiles-for-roguelike
* https://opengameart.org/content/sci-fi-interior-tiles
* https://opengameart.org/content/32x32-sci-fi-tileset

Misc Assets
-----------
* https://opengameart.org/content/hospital-topdown-perspective-2d-pixel-art
* https://opengameart.org/content/sci-fi-console-screen
* https://opengameart.org/content/sci-fi-object-set
* https://opengameart.org/content/190-pixel-art-assets-sci-fi-forest

Slime
-----
* https://opengameart.org/content/2d-pixel-slime-enemy-platformer-enemy
* https://opengameart.org/sites/default/files/slime%20spritesheet%20calciumtrice_0.png
