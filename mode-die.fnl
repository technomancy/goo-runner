(local (w h) (love.window.getMode))

{:draw (fn []
         (love.graphics.setColor 0 0 0)
         (love.graphics.rectangle "fill" 0 0 w h)
         (love.graphics.setColor 1 1 1)
         (love.graphics.print "GAME OVER" 100 100))
 :keypressed (fn [key set-mode]
               (if (= key "escape")
                   (love.event.quit)
                   (do (love.load)
                       (set-mode :play))))}
