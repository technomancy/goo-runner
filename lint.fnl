(local lume (require :lib.lume))

(local invisibles [:furniture-starts :enemy-starts :player :goal :patrols
                   :messages :blockers])

(fn [map]
  (let [layers {}]
    (each [_ layer (ipairs map.layers)]
      (tset layers layer.name layer))
    ;; it's easy to forget to set these layers to invisible in tiled.
    ;; why not just force them invisible here?
    (each [_ invisible (ipairs invisibles)]
      (tset (. layers invisible) :visible false))
    ;; make sure every enemy has a patrol-id that actually exists
    (let [patrols {}]
      (each [_ patrol (ipairs layers.patrols.objects)]
        (tset patrols patrol.id patrol))
      (each [_ enemy (ipairs layers.enemy-starts.objects)]
        (assert (. patrols enemy.properties.patrol-id)
                (.. "patrol not found: " enemy.properties.patrol-id))))
    map))
