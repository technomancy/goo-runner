(local lume (require :lib.lume))
(local utils (require :utils))

(local inventory {})

; Not type-safe, but suck a duck
(fn item? [a b]
  (if (and a.blob (= b.type "item"))
      b
      (and b.blob (= a.type "item"))
      a
      nil))

(fn get-fixture [world name]
  (let [body (lume.match (: world :getBodies)
                         (fn [b] (. (or (: b :getUserData) {}) :map)))]
    (lume.match (: body :getFixtures)
                (fn [f] (= (. (or (: f :getUserData) {}) :name) name)))))

(local sprites (love.graphics.newImage "assets/key-cards-small.png"))

(local quads
       {:blue (love.graphics.newQuad 0 0 32 32
                                     (: sprites :getDimensions))
        :red (love.graphics.newQuad 32 32 32 32
                                    (: sprites :getDimensions))
        :green (love.graphics.newQuad 0 32 32 32
                                      (: sprites :getDimensions))})

(fn draw [layer]
  (each [_ item (ipairs (. layer :objects))]
    (when item.visible
      ;; keycards are one tile below where they are in tiled for mystery reasons
      ;; could fix in here, but that wouldn't fix the collision boxes =\
      (love.graphics.draw sprites (. quads item.name) item.x item.y))))

(fn init [map world]
  (lume.clear inventory)
  (let [layer (utils.get-layer map :items)]
    (each [_ item (ipairs (. layer "objects"))]
      (set item.visible true))
    (set layer.draw (partial draw layer)))
  (each [name (pairs quads)]
    (when (get-fixture world name)
      (: (get-fixture world name) :setSensor false))))

(fn pickup [map world name]
  (let [layer (utils.get-layer map "items")]
    (each [_ item (ipairs (. layer "objects"))]
      (when (= item.name name)
        ;; save in inventory
        (tset inventory name true)
        ;; disable drawing
        (set item.visible false)))
    ;; doesn't actually remove it from the world; just stops collisions from
    ;; being blocking
    (: (get-fixture world name) :setSensor true)))

(fn openable? [door] (. inventory door.name))

(fn draw-hud []
  (var drawn 0)
  (each [keycard quad (pairs quads)]
    (when (. inventory keycard)
      (let [x 10 y (* drawn 32)]
        (love.graphics.draw sprites (. quads keycard) x y))
      (set drawn (+ drawn 1)))))

(fn has? [item-name] (. inventory item-name))

{:init init
 :item? item?
 :pickup pickup
 :openable? openable?
 :draw draw-hud
 :has? has?}
