(local lume (require :lib.lume))

(fn get-layer [map name]
  (lume.match map.layers (fn [l] (= l.name name))))

(fn get-bodies [world kind]
  (lume.filter (: world :getBodies)
               (fn [body] (. (or (: body :getUserData) {}) kind))))

(fn merge-body-user-data [body new-data]
 (: body :setUserData (lume.merge (: body :getUserData) new-data)))

(fn clear [world kind]
  (each [_ enemy (ipairs (get-bodies world kind))]
    (each [_ fixture (ipairs (: enemy :getFixtures))]
      (: fixture :destroy))
    (: enemy :destroy)))

{:get-layer get-layer
 :get-bodies get-bodies
 :clear clear
 :merge-body-user-data merge-body-user-data}
