(local repl (require :lib.stdio))
(local play (require :mode-play))
(local sound (require :sound))

(local canvas (love.graphics.newCanvas 720 450))
(local play (require "mode-play"))
(local font (love.graphics.newFont "assets/FSEX300.ttf" 16))

(var scale 2)

;; set the first mode
(var mode play)

(fn set-mode [mode-name ...]
  (set mode (require (.. "mode-" mode-name)))
  (when mode.activate
    (mode.activate ...)))

(fn love.load []
  (: canvas :setFilter "nearest" "nearest")
  (love.graphics.setFont font)
  (sound.play :music)
  (repl.start)
  (play.init))

(fn love.draw []
  (love.graphics.setCanvas canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1)
  (mode.draw)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1)
  (love.graphics.draw canvas 0 0 0 scale scale))

(fn love.update[dt]
  (when mode.update
    (mode.update dt set-mode scale)))

(fn love.touchmoved [...]
  (when mode.touchmoved
    (mode.touchmoved ...)))

(fn love.keypressed [key]
  (if
   (or (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "q"))
       (= key "escape"))
   (love.event.quit)
   (and (= key "f11") (= scale 2))
   (let [(dw dh) (love.window.getDesktopDimensions)]
     (love.window.setMode dw dh {:fullscreen true :fullscreentype :desktop})
     (set scale (/ dh 225)))

   (= key "m")
   (sound.toggle)

   (= key "f11")
   (do (set scale 2) (love.window.setMode 720 450 {:fullscreen false}))
   ;; add what each keypress should do in each mode
   (when mode.keypressed
     (mode.keypressed key set-mode))))
