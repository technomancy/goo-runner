(local lume (require :lib.lume))
(local patrol (require :patrol))
(local utils (require :utils))

(local (w h) (values 32 32))
(local sprites (love.graphics.newImage "assets/enemies.png"))
(local quads {:normal (love.graphics.newQuad 0 0 w h (: sprites :getDimensions))
              :fast (love.graphics.newQuad 0 96 w h (: sprites :getDimensions))})

(fn draw [world]
  (each [_ body (ipairs (utils.get-bodies world :enemy))]
    (let [(x y) (: body :getPosition)
          data (: body :getUserData)]
      (if data.pursuing
          (love.graphics.setColor 1 0.2 0.2)
          (love.graphics.setColor 1 1 1))
      (love.graphics.draw sprites (. quads data.type)
                          (math.floor x)
                          (math.floor y)
                          (- (: body :getAngle) (/ math.pi 2))
                          1 1 (/ w 2) (/ h 2)))))

(local shape (love.physics.newCircleShape 16))

(fn add-enemy [enemy world map]
  (let [body (love.physics.newBody world enemy.x enemy.y :dynamic)]
    (-> (love.physics.newFixture body shape)
        (: :setUserData {:enemy true}))
    (: body :setUserData {:patrol {:id enemy.properties.patrol-id}
                          :type enemy.type :range enemy.properties.range
                          :enemy true})
    (patrol.start-patrol body map)
    ;; for some reason setting the mass to <1 causes movement to bug out
    (: body :setMass 1)
    (: body :setLinearDamping 8)
    (: body :setAngularDamping 8)))

(fn init [map world]
  (utils.clear world :enemy)
  (let [(starts-layer layer-index) (utils.get-layer map "enemy-starts")
        layer (or (utils.get-layer map "enemies")
                  (: map :addCustomLayer "enemies" layer-index))]
    (each [_ enemy (ipairs (. starts-layer "objects"))]
      (add-enemy enemy world map))
    (set layer.draw (partial draw world))))

(local range 96)

(fn update [map world blob dt]
  (let [(bx by) (: blob.centerBody :getPosition)]
    (each [_ enemy (ipairs (utils.get-bodies world :enemy))]
      (let [(ex ey) (: enemy :getPosition)
            dist (lume.distance ex ey bx by)
            data (: enemy :getUserData)]
        ;; keep some state around; don't immediately disengage unless the
        ;; blob moves out of the larger range
        (if (< dist (or data.range range))
            (set data.pursuing true)
            (> dist (* range 1.25))
            (set data.pursuing nil))
        (patrol.update-patrol enemy map (if data.pursuing {:x bx :y by}))))))

{:init init :update update}
