VERSION=0.2.0
NAME=goo-runner
URL=https://technomancy.itch.io/goo-runner
AUTHOR="Phil Hagelberg and Emma Bukacek"
DESCRIPTION="You are a slime! Escape from the lab!"

LIBS := $(wildcard lib/*)
LUA := $(wildcard *.lua)
SRC := $(wildcard *.fnl)
OUT := $(patsubst %.fnl,%.lua,$(SRC))

LOVEFILE=releases/$(NAME)-$(VERSION).love

run: ; love $(PWD)

%.lua: %.fnl ; lua lib/fennel --compile --correlate $< > $@
clean: ; rm -rf $(OUT)

$(LOVEFILE): $(LUA) $(OUT) $(LIBS) assets
	mkdir -p releases/
	find $^ -type f | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@

love: $(LOVEFILE)

# platform-specific distributables

REL="$(PWD)/love-release.sh" # https://p.hagelb.org/love-release.sh
FLAGS=-a "$(AUTHOR)" --description $(DESCRIPTION) -t "$(NAME)"\
	--love 11.1 --url $(URL) --version $(VERSION) --lovefile $(LOVEFILE)

releases/$(NAME)-$(VERSION)-x86_64.AppImage: $(LOVEFILE)
	cd appimage && ./build.sh 11.1 $(PWD)/$(LOVEFILE)
	mv appimage/game-x86_64.AppImage $@

releases/$(NAME)-$(VERSION)-macos.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -M
	mv releases/goo-runner-macos.zip $@

releases/$(NAME)-$(VERSION)-win.zip: $(LOVEFILE)
	OUT="$(NAME)-$(VERSION).love" $(REL) $(FLAGS) -W32
	mv releases/goo-runner-win32.zip $@

linux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
mac: releases/$(NAME)-$(VERSION)-macos.zip
windows: releases/$(NAME)-$(VERSION)-win.zip

uploadlinux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
	butler push $^ technomancy/goo-runner:linux
uploadmac: releases/$(NAME)-$(VERSION)-macos.zip
	butler push $^ technomancy/goo-runner:mac
uploadwindows: releases/$(NAME)-$(VERSION)-win.zip
	butler push $^ technomancy/goo-runner:windows

upload: uploadlinux uploadmac uploadwindows
