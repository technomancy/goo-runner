love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Goo Runner", "goo-runner"
   t.modules.joystick, t.modules.video = false, false
   t.window.width = 720
   t.window.height = 450
   t.window.vsync = false
   t.version = "11.1"
end
